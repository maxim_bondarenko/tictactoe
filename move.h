#ifndef MOVE_H
#define MOVE_H

struct Cell {
    Cell(int row, int col) : row(row), col(col) {}
    bool operator !=(const Cell &other) { return row != other.row || col != other.col; }
    int row;
    int col;
};

struct Move {
    enum MoveResult { DefeatInOneMove = 0,
                      DefeatPossible = 1,
                      StandoffPossible = 2,
                      VictoryPossible = 3,
                      VictoryInOneMove = 4 };

    Move(int row, int col, MoveResult r) : row(row), col(col), result(r) {}
    Move(const Cell &c, MoveResult r) : row(c.row), col(c.col), result(r) {}

    int row;
    int col;
    MoveResult result;
};


#endif // MOVE_H
