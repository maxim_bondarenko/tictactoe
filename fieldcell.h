#ifndef FIELDCELL_H
#define FIELDCELL_H

#include <QWidget>
#include <QImage>
#include "cellimagesrc.h"

class FieldCell : public QWidget
{
    Q_OBJECT
public:
    enum CellState { Zero, Cross, Empty };

    FieldCell(int x, int y, CellImageSrc *imageSrc, QWidget *parent = 0);

    void setState(CellState newState, bool isRepaint = true);
    CellState getState();

protected:
    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    QSize sizeHint() const;

signals:
    void clicked(int x, int y);

public slots:

private:
    QImage *image;
    CellImageSrc *imageSrc;
    int x;
    int y;

    CellState state;

    bool pressed;
};

#endif // FIELDCELL_H
