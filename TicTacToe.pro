#-------------------------------------------------
#
# Project created by QtCreator 2012-03-16T09:27:39
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = TicTacToe
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    fieldcell.cpp \
    tttfield.cpp \
    cellstates.cpp \
    gifcellimagesrc.cpp \

HEADERS  += widget.h \
    fieldcell.h \
    tttfield.h \
    cellstates.h \
    Move.h \
    cellimagesrc.h \
    gifcellimagesrc.h

RESOURCES += \
    TicTacToe.qrc
