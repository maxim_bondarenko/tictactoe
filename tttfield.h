#ifndef TTTFIELD_H
#define TTTFIELD_H

#include <QWidget>
#include "fieldcell.h"
#include "move.h"
#include "cellstates.h"
#include "cellimagesrc.h"
#include <vector>

/*struct Move {
    enum MoveResult { DefeatInOneMove = 0,
                      DefeatPossible = 1,
                      StandoffPossible = 2,
                      VictoryPossible = 3,
                      VictoryInOneMove = 4 };

    Move(int row, int col, MoveResult r) :  row(row), col(col), result(r) {}

    int row;
    int col;
    MoveResult result;
};*/

class TTTField : public QWidget
{
    Q_OBJECT
public:
    //enum CellState { Zero, Cross, Empty };
    typedef FieldCell::CellState CellState;
    enum { FIELD_SIZE = 3, LINE_WIDTH = 4, BORDER_WIDTH = 0 };
    enum GameMode { HumanFirst = 1, ComputerFirst = 2 };
    enum GameResult { HumanWon, ComputerWon, StandOff };

public:
    explicit TTTField(QWidget *parent = 0);
    ~TTTField();
    
    void newGame(GameMode);
protected:
    void paintEvent(QPaintEvent * event);
signals:
    void endGame(int result);
public slots:
    void cellChecked(int row, int col);
private:
    Move findMyBestMove(CellStates cells);
    Move findRivalBestMove(CellStates cells);

    void makeMove();

    CellImageSrc *imageSrc;
    std::vector< std::vector< FieldCell* > > cells;
    CellState mySimbol;
    CellState rivalSimbol;
};

#endif // TTTFIELD_H
