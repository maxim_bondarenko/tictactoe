#pragma once

#include "cellimagesrc.h"

class GifCellImageSrc : public CellImageSrc {
public:
    explicit GifCellImageSrc(const std::string& zero_id, const std::string& cross_id);

    QImage* getZero() const;
    QImage* getCross() const;

private:
    QImage* zero;
    QImage* cross;
};
