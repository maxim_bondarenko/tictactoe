#ifndef CELLSTATES_H
#define CELLSTATES_H

#define SOME_SIZE 1

#include "move.h"
#include "fieldcell.h"
#include <vector>


class CellStates {
public:
    CellStates(std::vector< std::vector<FieldCell*> > cells, int size);

    CellStates(const CellStates &other, const Cell &c, FieldCell::CellState newState);
    CellStates(const CellStates &other);


    const CellStates &operator =(const CellStates &other);

    FieldCell::CellState cell(int row, int col) const;

    bool fullLineExist(FieldCell::CellState);
    bool emptyCellExist();

    Cell firstEmpty();
    Cell nextEmpty(const Cell &prev);
    Cell endEmpty();
private:
    void initStorage(int size);

    std::vector< std::vector<FieldCell::CellState> > mCells;
    int size;
};

#endif // CELLSTATES_H
