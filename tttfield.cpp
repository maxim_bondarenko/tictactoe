#include "tttfield.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <set>
#include <QMessageBox>
#include <QPainter>
#include <vector>
#include <sstream>

#include "gifcellimagesrc.h"

TTTField::TTTField(QWidget *parent) :
    QWidget(parent),
    mySimbol(FieldCell::Cross),
    rivalSimbol(FieldCell::Zero)
{
    cells.resize(FIELD_SIZE);
    for ( int i = 0; i < FIELD_SIZE; i++ ) {
        cells[i].resize(FIELD_SIZE);
    }
    imageSrc = new GifCellImageSrc(":/zero01.gif", ":/cross01.gif");

    QVBoxLayout *vLayout = new QVBoxLayout(this);
    setLayout(vLayout);
    vLayout->setContentsMargins(BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH);
    vLayout->setSpacing(LINE_WIDTH);

    for ( int row = 0; row < FIELD_SIZE; row++ ) {
        QHBoxLayout *hLayout = new QHBoxLayout();
        vLayout->addLayout(hLayout);
        hLayout->setContentsMargins(BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH);
        hLayout->setSpacing(LINE_WIDTH);

        for ( int col = 0; col < FIELD_SIZE; col++ ) {
            FieldCell *newCell = new FieldCell(row, col, imageSrc, this);
            hLayout->addWidget(newCell);
            cells[row][col] = newCell;
            //cellStates[row][col] = FieldCell::Empty;
            connect(newCell, SIGNAL(clicked(int,int)), this, SLOT(cellChecked(int,int)));
        }
    }
    this->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

TTTField::~TTTField() {
    delete imageSrc;
}

void TTTField::newGame(GameMode mode) {
    for ( int row = 0; row < FIELD_SIZE; row++ ) {
        for ( int col = 0; col < FIELD_SIZE; col++ ) {
            cells[row][col]->setState(FieldCell::Empty, false);
        }
    }

    repaint();
    // imageSrc->reset();

    if ( mode == ComputerFirst ) {
        makeMove();
    }
}

void TTTField::paintEvent(QPaintEvent *event) {
    QWidget::paintEvent(event);
    //int left, top, right, bottom;

    QPainter painter(this);
    QColor fillColor(0, 0, 50);
    QRect gameRect(rect());

    painter.fillRect(gameRect, fillColor);
}

void TTTField::makeMove() {
    CellStates states( cells, FIELD_SIZE );

    Move move = findMyBestMove(states);
    FieldCell* cell = cells[move.row][move.col];
    cell->setState(mySimbol);
}

void TTTField::cellChecked(int row, int col) {
    cells[row][col]->setState(rivalSimbol);
    CellStates states(cells, FIELD_SIZE);

    if ( states.fullLineExist(rivalSimbol) ) {
        endGame(HumanWon);
        return;
    }
    if ( ! states.emptyCellExist() ) {
        endGame(StandOff);
        return;
    }

    makeMove();

    CellStates newStates(cells, FIELD_SIZE);
    if ( newStates.fullLineExist(mySimbol) ) {
        endGame(ComputerWon);
    } else if ( ! newStates.emptyCellExist() ) {
        endGame(StandOff);
    }
}

Move TTTField::findMyBestMove(CellStates cells) {
    std::vector<Move> moves;
    std::vector<Cell> emptyCells;
    int bestMove = 0;

    emptyCells.reserve(9);
    Cell end = cells.endEmpty();
    for ( Cell c(cells.firstEmpty()); c != end; c = cells.nextEmpty(c) ) {
        CellStates candidate(cells, c, mySimbol);
        if ( candidate.fullLineExist(mySimbol) ) {
            return Move(c, Move::VictoryInOneMove);
        } else {
            emptyCells.push_back(c);
        }
    }

    int len = emptyCells.size();

    if ( len == 0 ) {
        return Move(0, 0, Move::StandoffPossible);
    }
    moves.reserve(len);
    for ( int i = 0; i < len; i++ ) {
        Cell c(emptyCells[i]);
        Move nextMove = findRivalBestMove(CellStates(cells, c, mySimbol));
        Move move(c, nextMove.result);
        if ( move.result == Move::VictoryInOneMove ) {
            return move;
            //move.result = Move::VictoryPossible;
        }
        if ( i != bestMove && move.result > moves[bestMove].result ) {
            bestMove = i;
        }
        moves.push_back(move);
    }

    return moves[bestMove];
}

Move TTTField::findRivalBestMove(CellStates cells) {
    std::vector<Move> moves;
    std::vector<Cell> emptyCells;
    //std::vector<CellStates> candidates;
    int bestMove = 0;

    emptyCells.reserve(9);
    Cell end = cells.endEmpty();
    for ( Cell c(cells.firstEmpty()); c != end; c = cells.nextEmpty(c) ) {
        CellStates candidate(cells, c, rivalSimbol);
        if ( candidate.fullLineExist(rivalSimbol) ) {
            return Move(c, Move::DefeatInOneMove);
        } else {
            emptyCells.push_back(c);
            //moves.push_back(c, Move::StandoffPossible);
        }
    }

    int len = emptyCells.size();

    if ( len == 0 ) {
        return Move(0, 0, Move::StandoffPossible);
    }

    moves.reserve(len);
    for ( int i = 0; i < len; i++ ) {
        Cell c(emptyCells[i]);
        Move nextMove = findMyBestMove(CellStates(cells, c, rivalSimbol));
        Move move(c, nextMove.result);
        if ( move.result == Move::DefeatInOneMove ) {
            return move;
            //move.result = Move::DefeatPossible;
        }
        if ( i != bestMove && move.result < moves[bestMove].result ) {
            bestMove = i;
        }
        moves.push_back(move);
    }

    return moves[bestMove];
}
