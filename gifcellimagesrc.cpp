#include "gifcellimagesrc.h"

GifCellImageSrc::GifCellImageSrc(const std::string& zero_id, const std::string& cross_id) {
    zero = new QImage(zero_id.data());
    cross = new QImage(cross_id.data());
}

QImage* GifCellImageSrc::getZero() const {
    return zero;
}

QImage* GifCellImageSrc::getCross() const {
    return cross;
}
