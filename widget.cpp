#include "widget.h"
#include "fieldcell.h"
#include <QHBoxLayout>
#include <QLabel>
#include "tttfield.h"
#include <QMessageBox>
#include <sstream>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    QHBoxLayout *layout = new QHBoxLayout(this);
    setLayout(layout);
    gameField = new TTTField(this);
    layout->addWidget(gameField);

    connect(gameField, SIGNAL(endGame(int)), this, SLOT(onGameEnd(int)));
}

Widget::~Widget()
{
    
}

void Widget::onGameEnd(int result) {
    QString msg;
    switch ( result ) {
    case TTTField::ComputerWon:
        msg += "Sorry, I won!";
        break;
    case TTTField::HumanWon:
        msg += "Congratualtions, you won!";
        break;
    case TTTField::StandOff:
        msg += "Thank you for good contest, stand-off!";
        break;
    default:
        break;
    }

    QMessageBox msgBox;
    msgBox.addButton("New game, Human first", QMessageBox::YesRole);
    msgBox.addButton("New game, AI first", QMessageBox::NoRole);
    msgBox.addButton("Quit", QMessageBox::RejectRole);
    msgBox.setText(msg + "\nWhat would yout like to do?");
    int ret = msgBox.exec();
    std::stringstream out;
    out << ret;
    switch ( ret ) {
    case 0: // Human first
        gameField->newGame(TTTField::HumanFirst);
        break;
    case 1: // Computer first
        gameField->newGame(TTTField::ComputerFirst);
        break;
    default:
        close();
        break;
    }
}
