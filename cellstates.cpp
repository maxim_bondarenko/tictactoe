#include "cellstates.h"

//template <class MatrixStorage>
CellStates::CellStates(std::vector< std::vector<FieldCell*> > cells, int size) : size(size) {
    initStorage(size);
    for ( int row = 0; row < size; row++ ) {
        for ( int col = 0; col < size; col++ ) {
            mCells[row][col] = cells[row][col]->getState();
        }
    }
}

CellStates::CellStates(const CellStates &other, const Cell &c, FieldCell::CellState newState) {
    *this = other;
    mCells[c.row][c.col] = newState;
}

/*CellStates::CellStates(CellState cells[SOME_SIZE][SOME_SIZE], int size) : size(size) {
    for ( int row = 0; row < size; row++ ) {
        for ( int col = 0; col < size; col++ ) {
            mCells[row][col] = cells[row][col];
        }
    }
}*/

CellStates::CellStates(const CellStates &other) : size(other.size) {
    initStorage(size);
    for ( int row = 0; row < size; row++ ) {
        for ( int col = 0; col < size; col++ ) {
            mCells[row][col] = other.mCells[row][col];
        }
    }
}

const CellStates &CellStates::operator =(const CellStates &other) {
    size = other.size;
    initStorage(size);
    for ( int row = 0; row < size; row++ ) {
        for ( int col = 0; col < size; col++ ) {
            mCells[row][col] = other.mCells[row][col];
        }
    }
    return *this;
}

void CellStates::initStorage(int size) {
    mCells.resize(size);
    for ( int i = 0; i < size; i++ ) {
        mCells[i].resize(size);
    }
}

FieldCell::CellState CellStates::cell(int row, int col) const {
    return mCells[row][col];
}

Cell CellStates::firstEmpty() {
    for ( int row = 0; row < size; row++ ) {
        for ( int col = 0; col < size; col++ ) {
            if ( mCells[row][col] == FieldCell::Empty ) {
                return Cell(row, col);
            }
        }
    }

    return Cell(-1, -1);
}

Cell CellStates::nextEmpty(const Cell &prev) {
    int row = prev.row;
    int col = prev.col + 1;
    if ( col >= size ) {
        row +=1;
        col = 0;
    }
    for ( ; row < size; row++ ) {
        for ( ; col < size; col++ ) {
            if ( mCells[row][col] == FieldCell::Empty ) {
                return Cell(row, col);
            }
        }
        col = 0;
    }

    return Cell(-1, -1);
}

Cell CellStates::endEmpty() {
    return Cell(-1, -1);
}

bool CellStates::fullLineExist(FieldCell::CellState state) {
    bool lineFound = false;

    // search for horizontal line
    for ( int row = 0; row < size && ! lineFound; row++ ) {
        bool wrongCellFound = false;

        for ( int col = 0; col < size && ! wrongCellFound; col++ ) {
            if ( mCells[row][col] != state ) {
                wrongCellFound = true;
            }
        }
        if ( ! wrongCellFound ) {
            return true;
        }
    }

    // search for vertical line
    for ( int col = 0; col < size && ! lineFound; col++ ) {
        bool wrongCellFound = false;

        for ( int row = 0; row < size && ! wrongCellFound; row++ ) {
            if ( mCells[row][col] != state ) {
                wrongCellFound = true;
            }
        }
        if ( ! wrongCellFound ) {
            return true;
        }
    }

    // search for diagonal line
    bool wrongCellFound = false;
    for ( int row = 0, col = 0; col < size && ! wrongCellFound; col++, row++ ) {
        if ( mCells[row][col] != state ) {
            wrongCellFound = true;
        }
    }
    if ( ! wrongCellFound ) {
        return true;
    }

    // search for back diagonal line
    wrongCellFound = false;
    for ( int row = 0, col = size - 1; col >= 0 && ! wrongCellFound; row++, col-- ) {
        if ( mCells[row][col] != state ) {
            wrongCellFound = true;
        }
    }
    if ( ! wrongCellFound ) {
        return true;
    }

    return false;
}

bool CellStates::emptyCellExist() {
    return firstEmpty() != endEmpty();
}
