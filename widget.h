#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "tttfield.h"

class Widget : public QWidget
{
    Q_OBJECT
public slots:
    void onGameEnd(int result);
public:
    Widget(QWidget *parent = 0);
    ~Widget();
private:
    TTTField *gameField;
};

#endif // WIDGET_H
