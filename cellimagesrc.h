#pragma once

#include <QImage>

class CellImageSrc {
public:
    virtual ~CellImageSrc() = default;

    virtual QImage* getZero() const = 0;
    virtual QImage* getCross() const = 0;
};
