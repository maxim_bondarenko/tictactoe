#include "fieldcell.h"
#include <QPainter>
#include <QMessageBox>
#include <QLabel>

FieldCell::FieldCell(int x, int y, CellImageSrc *imageSrc, QWidget *parent) :
    QWidget(parent),
    imageSrc(imageSrc),
    x(x),
    y(y),
    state(Empty),
    image(NULL)
{
}

void FieldCell::paintEvent(QPaintEvent *event) {
    QWidget::paintEvent(event);

    QPainter painter(this);
    QColor fillColor(250, 250, 250);
    painter.fillRect(rect(), fillColor);

    if ( state != Empty ) {
        painter.drawImage(0, 0, *image);
    }
}

QSize FieldCell::sizeHint() const {
    return QSize(64, 64);
}

void FieldCell::mousePressEvent(QMouseEvent *) {
    if ( state == Empty ) {
        pressed = true;
    }
}

void FieldCell::mouseReleaseEvent(QMouseEvent *) {
    if ( pressed ) {
        pressed = false;
        clicked(x, y);
    }
}

void FieldCell::setState(CellState newState, bool isRepaint) {
    state = newState;
    if ( newState == Zero ) {
        // delete image;
        image = imageSrc->getZero();
    } else if ( newState == Cross ){
        // delete image;
        image = imageSrc->getCross();
    }
    if ( isRepaint ) {
        this->repaint();
    }
}

FieldCell::CellState FieldCell::getState() {
    return state;
}
